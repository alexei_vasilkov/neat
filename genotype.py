import numpy as np

def choose_shortest_and_longest(q1, q2):
    if len(q1) > len(q2):
        return q2, q1
    else:
        return q1, q2

class Gene:
    """
    Gene tuple
    """
    def __init__(self, gene_key, weight, enabled, innov):
        self.in_ = gene_key[0]
        self.out = gene_key[1]
        self.weight = weight
        self.enabled = enabled
        self.innov = innov
        self.hash_key = gene_key

    def __str__(self):
        g = self
        s = 'Connection %d -> %d:\n' % (g.in_, g.out)
        s += 'In: %d\nOut: %d\nWeight: %0.5f\nEnabled: %r\nInnovation_n: %d\n' %\
                (g.in_, g.out, g.weight, g.enabled, g.innov)
        return s

    def copy(self):
        """ copy itself """
        return Gene(self.hash_key, self.weight, self.enabled, self.innov)

class Genotype:
    """
    Genome structure is here.
    """
    NODE_TYPE_TO_HUMAN = {0 : 'Sensor',
                          1 : 'Hidden',
                          2 : 'Output'
                         }
    NODE_TYPES = (0, 1, 2, None)

    NODE_TYPE_TO_NEURON = {0 : 'Constant',
                           1 : 'Hidden',
                           2 : 'Output',
                           None : None
                          }
    def __init__(self, init_number):
        #init_tuple = in_dim, out_dim + in_dim + 1
        #TODO different types of neurons
        #if hidden, s
        #{.., id : node_type, ..}
        self.nodes = set([])
        #not counting outputs
        self.max_node_id = 0
        #TODO think about it
        #(.., (In, Out) : (In, Out, Weight, Enabled, Innov), ..)
        self.genes = {}
        self.genes_by_innov = {}
        #TODO keep track
        self.max_innov = -1
        #TODO test it on larger tasks
        #if len()=N < 20 make N=1.0
        #set it to False if not recurrent even on rec it's not good
        #but not by 2 times
        self.make_small_more_diverse = True
        self.fitness = 0.0001
        self.genes_in_list = []
        self.nodes_in_list = []
        self.firing_order = {}

        self.init_in_out_nodes(init_number)

    def dummy_representative(self):
        rep = Genotype(0)
        genes = {}
        innovs = {}
        for key, gene in self.genes.items():
            g = gene.copy()
            genes[key] = g
            innovs[g.innov] = g
        rep.genes = genes
        rep.genes_by_innov = innovs
        rep.max_innov = self.max_innov
        return rep

    def copy(self, init_tuple):
        ng = Genotype(init_tuple)
        ng.fitness = self.fitness
        for gene in self.genes.values():
            ng.add_gene(gene.copy())
        return ng

    def change_gene_expression(self, hash_key, enabled):
        """
        enable/disable a gene
        and update inputs and outputs in neurons
        """
        self.genes[hash_key].enabled = enabled

    def add_gene(self, gene):
        """
        adds a new gene and a new node if needed
        """
        if gene.hash_key in self.genes:
            return
            raise
        if self.max_innov < gene.innov:
            self.max_innov = gene.innov
        hash_key = gene.hash_key
        self.genes[hash_key] = gene
        self.genes_by_innov[gene.innov] = gene
        for n_id in hash_key:
            if n_id not in self.nodes:
                self.add_node(n_id)
        #order is not guaranteed
        self.genes_in_list.append(hash_key)

    def init_in_out_nodes(self, init_number):
        """
        init_tuple = out_dim + in_dim
        """
        #first inputs then bias then outputs
        for i in range(init_number):
            self.add_node(i)

    def add_node(self, neuron_id=None):
        if neuron_id is None:
            self.max_node_id += 1
            neuron_id = self.max_node_id
            if self.max_node_id in self.nodes:
                raise
            self.nodes.add(self.max_node_id)
        else:
            if neuron_id in self.nodes:
                raise
            self.nodes.add(neuron_id)
            if neuron_id > self.max_node_id:
                self.max_node_id = neuron_id
        self.nodes_in_list.append(neuron_id)
        return neuron_id

    def __len__(self):
        return len(self.genes)

    #tested
    def get_distance_values(self, another):
        """
        Computes excesses, disjoints and weight differences average
        """
        #somewhat tested
        #count disjoints
        shortest, longest = choose_shortest_and_longest(self, another)

        matching_number = 0

        weight_diff_ave = 0

        for innov in shortest.genes_by_innov:
            if innov in longest.genes_by_innov:
                matching_number += 1
                #including disabled
                weight_diff_ave += abs(shortest.genes_by_innov[innov].weight -
                                       longest.genes_by_innov[innov].weight)

        if matching_number > 0:
            weight_diff_ave /= matching_number

        excess_and_disjoint_n = len(self) + len(another) - matching_number*2

        #count disjoints
        with_min_inno = self
        with_max_inno = another
        if with_min_inno.max_innov > with_max_inno.max_innov:
            with_min_inno, with_max_inno = with_max_inno, with_min_inno

        disjoint_number = len(with_min_inno) - matching_number*2
        min_inno = with_min_inno.max_innov

        for innov in with_max_inno.genes_by_innov:
            if min_inno + 1 > innov:
                disjoint_number += 1

        excess_number = excess_and_disjoint_n - disjoint_number
        largest_len = len(longest)
        #TODO be careful here
        #for xor non recurrent it made solution two times as bad and long
        if self.make_small_more_diverse and largest_len < 20:
            largest_len = 1.0
        return np.array([excess_number/largest_len,
                         disjoint_number/largest_len, weight_diff_ave]).T

    def __str__(self):
        s = 'Max innovation: %d\n' % self.max_innov
        s += 'Node genes(%d):\n' % len(self.nodes)
        if len(self.nodes) == 0:
            s += 'Empty\n'
        for node in sorted(self.nodes):
            s += 'Node %d :\n' % node
        s += '\nConnectivity genes(%d):\n' % len(self)
        if len(self) == 0:
            s += 'Empty\n'
        for g in sorted(self.genes_by_innov.values(), key=lambda x: (x.out, x.in_)):
            s += str(g)
        return s + '\n'


