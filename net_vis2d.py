from graph_tool.all import Graph, graph_draw
import math
def float_rgb(mag, cmin, cmax):
       """
       Return a tuple of floats between 0 and 1 for the red, green and
       blue amplitudes.
       """

       try:
              # normalize to [0,1]
              x = float(mag-cmin)/float(cmax-cmin)
       except:
              # cmax = cmin
              x = 0.5
       blue = min((max((4*(0.75-x), 0.)), 1.))
       red  = min((max((4*(x-0.25), 0.)), 1.))
       green= min((max((4*math.fabs(x-0.5)-1., 0.)), 1.))
       return [red, green, blue, 1.]

class NEATVis:

    def __init__(self, in_dim, out_dim):
        self.sensor_node_c = [0, 1, 68/255, 1]
        self.bias_node_c = [92/255, 92/255, 1, 1]
        self.output_node_c = [250/255, 0, 0, 1]
        self.hidden_node_c = [171/255, 164/255, 116/255, 1]
        self.font_size = 25
        self.in_dim = in_dim
        self.out_dim = out_dim

    def visualize(self, genotype):
        g = Graph()
        nodes = {}
        node_colors = g.new_vertex_property("vector<double>")
        edge_colors = g.new_edge_property("vector<double>")
        pos = g.new_vertex_property("vector<double>")
        ln = len(genotype.nodes)
        per_row = (self.in_dim - 1) if self.in_dim > self.out_dim else (self.out_dim - 1)
        #if per_row < 5:
        #    per_row = 5
        per_row = 10
        row_count = math.ceil((ln - self.in_dim - self.out_dim) / per_row) + 2
        row_offset = 0
        if row_count < 5:
            row_count = 5
        for i, node in enumerate(genotype.nodes):
            vertex = g.add_vertex()
            node = i
            pos[vertex] = (i, 1)
            if node < self.in_dim - 1:
                node_color = self.sensor_node_c
                if self.in_dim < self.out_dim:
                    ratio = self.out_dim/self.in_dim
                else:
                    ratio = 1
                offset = 0
                if self.in_dim < per_row:
                    offset = per_row/2 - self.in_dim*ratio/2
                pos[vertex] = (i * ratio + offset, row_count - 1)
            elif node == self.in_dim - 1:
                node_color = self.bias_node_c
                if self.in_dim < self.out_dim:
                    ratio = self.out_dim/self.in_dim
                else:
                    ratio = 1
                offset = 0
                if self.in_dim < per_row:
                    offset = per_row/2 - self.in_dim*ratio/2
                pos[vertex] = (offset + i * ratio, row_count - 1)
            elif node < self.in_dim + self.out_dim:
                node_color = self.output_node_c
                if self.in_dim > self.out_dim:
                    ratio = self.in_dim/self.out_dim
                else:
                    ratio = 1
                pos[vertex] = ((i - self.in_dim) * ratio + ratio/self.out_dim, 0)
            else:
                node_color = self.hidden_node_c
                i_ = i - self.in_dim - self.out_dim
                width = 1
                if ((i - self.in_dim - self.out_dim) // per_row + 1)*per_row > ln - self.in_dim -\
                        self.out_dim:
                    width = per_row/((ln - self.in_dim - self.out_dim) - ((i - self.in_dim -
                        self.out_dim) // per_row)*per_row)
                pos[vertex] = ((0.5 + (i_) % per_row)*width, row_count  - row_offset - 2 - ((i_) // per_row))


            nodes[node] = vertex
            node_colors[vertex] = node_color
        min_w = 1000000000
        max_w = -1000000000
        for gene in genotype.genes.values():
            if gene.enabled:
                if min_w > gene.weight:
                    min_w = gene.weight
                if max_w < gene.weight:
                    max_w = gene.weight

        for gene in genotype.genes.values():
            if gene.enabled:
                edge = g.add_edge(nodes[gene.in_], nodes[gene.out])
                edge_colors[edge] = float_rgb(gene.weight, min_w, max_w)
        graph_labels = g.new_vertex_property("string")
        for node_id, vertex in nodes.items():
            graph_labels[vertex] = str(node_id)

        graph_draw(g, pos=pos,
                   vertex_text=graph_labels,
                   bg_color = [1, 1, 1, 1],
                   vertex_font_size=self.font_size,
                   vertex_color=node_colors,
                   edge_color=edge_colors,
                   output_size=(1920, 1280))
                   #output='test_image.png')


