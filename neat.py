"""
My implementation of NEAT paper.
"""
import itertools
import numpy as np
import random
from population import Population
from genotype import Gene, Genotype
#from net_vis import visualize
import time
#from net_vis2d import NEATVis
import pickle
from phenotype import Network


class Report:
    """
    gathering and displaying evolution progress
    """

    def __init__(self, nof_sims):
        self.nof_sims = nof_sims
        self.speciation = [[] for i in range(nof_sims)]
        self.evaluating_time = [[] for i in range(nof_sims)]
        self.alg_time = [[] for i in range(nof_sims)]
        self.champs_info = [[] for i in range(nof_sims)]
        self.pop_aves = [[] for i in range(nof_sims)]

    def add_generation(self, sim_id, info):
        self.speciation[sim_id].append(info['speciation'])
        self.evaluating_time[sim_id].append(info['evaluating_time'])
        self.alg_time[sim_id].append(info['alg_time'])
        self.pop_aves[sim_id].append(info['pop_ave'])
        self.champs_info[sim_id] = 0

    def visualize(self):
        pass

def count_not_disabled_genes(genotype):
    res = 0
    for gene in genotype.genes.values():
        if gene.enabled:
            res += 1
    return res

def secs_to_time(seconds):
    m, s = divmod(seconds, 60)
    h, m = divmod(m, 60)
    return "%d:%02d:%2.1f" % (h, m, s)

def time_elapsed(time_before, raw=False):
    if not raw:
        return secs_to_time(time.time() - time_before)
    else:
        return secs_to_time(time_before)

class NeatLearning:

    def __init__(self, nof_sims, in_dim, out_dim, population_size, fitness_f,
                 desirable_fitness, recurrent=True, multicore=False, pop_params={},
                 check_for_champ=None):
        self.in_dim = in_dim
        self.out_dim = out_dim
        self.recurrent = recurrent
        self.fitness_f = fitness_f
        #gen_id, fitness, genotype
        self.best_result = (0, 0, None)
        self.multicore = multicore
        self.continue_running_after_solution = False
        self.max_gens = 1000
        self.nof_sims = nof_sims
        #solution will look for exact value or larger
        self.desirable_fitness = desirable_fitness
        self.check_for_champ = check_for_champ
        self.population_size = population_size
        self.init_params(pop_params)
        self.save_best_sims = True
        self.verbose = False

    def init_params(self, pop_params):
        self.pop_params = {}
        mutations_chances = {}
        mutations_chances['add_connection'] = 0.05
        mutations_chances['add_node'] = 0.03
        mutations_chances['mutate_weights'] = 0.8
        mutations_chances['perturb_weight'] = 0.9
        mutations_chances['disabled_inheritance'] = 0.75
        mutations_chances['interspecies_mating'] = 0.001

        self.pop_params['mutations_chances'] = mutations_chances

        self.pop_params['dist_coeffs'] = np.array([1.0, 1.0, 0.4])
        self.pop_params['compat_th'] = 3.0
        self.pop_params['champs_percent_to_reproduce'] = 0.4
        self.pop_params['offspring_percent_wo_crossover'] = 0.25
        self.pop_params['perturbation_step'] = 0.1
        self.pop_params['progress_ratio_th_to_kill_species'] = 1.0001
        self.pop_params['progress_ratio_th_for_martial_law'] = 1.0001

        if 'mutations_chances' in pop_params:
            new_m_c = pop_params['mutations_chances']
            for key in new_m_c:
                mutations_chances[key] = new_m_c[key]
            del pop_params['mutations_chances']

        for param in pop_params:
            self.pop_params[param] = pop_params[param]

    def find_solution(self, sim_id):
        sim_best_result = (0, 0, None)
        reptort = []
        population = Population(self.population_size,
                                self.in_dim, self.out_dim,
                                self.fitness_f,
                                self.recurrent,
                                self.multicore)
        population.change_default_params(self.pop_params)
        solution_found = False
        prev_best = 0
        for gen_id in range(1, self.max_gens + 1):
            time_before = time.time()
            population.next_gen()
            #best not in generation but in entire simulation
            if sim_best_result[1] < population.get_max_fitness():
                sim_best_result = (gen_id, population.get_max_fitness(),
                               population.get_the_most_fittest())
            if self.verbose:
                print('Sim %d, Generation %d completed.' % (sim_id, gen_id))
                print('Time elapsed:', time_elapsed(time_before))
                print('Max fitness = %0.5f' % sim_best_result[1])
            if self.desirable_fitness is not None and self.check_for_champ is None and\
                    population.get_max_fitness() >= self.desirable_fitness:
                if not self.continue_running_after_solution:
                    solution_found = True
                    break
            elif self.check_for_champ is not None:
                if self.desirable_fitness is not None:
                    if sim_best_result[1] < self.desirable_fitness:
                        continue
                if prev_best == sim_best_result[1]:
                    continue
                pickle.dump(sim_best_result, open('notachamp%0.1f' % sim_best_result[1], 'wb'))
                if self.check_for_champ(Network(sim_best_result[2], self.in_dim + 1, self.out_dim, self.recurrent)) and not self.continue_running_after_solution:
                    solution_found = True
                    break
            prev_best = sim_best_result[1]
        self.best_result = sim_best_result
        return solution_found

    def start_simulations(self, nof_sims=0):
        if nof_sims == 0:
            nof_sims = self.nof_sims
        champs = [[], [], []]
        total_time = 0
        fails = 0
        for sim_id in range(1, nof_sims + 1):
            print('\nRunning simulation %d' % sim_id)
            time_before = time.time()
            if self.find_solution(sim_id):
                print('Solution has been found successfully')
                print('The best result after %d generations is %0.2f' % (self.best_result[0],
                                                                         self.best_result[1]))
                champs[0].append(self.best_result[0])
                champs[1].append(self.best_result[1])
                champs[2].append(self.best_result[2])
            else:
                print('\nUnsuccessfull simulation')
                fails += 1
            print('Time elapsed:', time_elapsed(time_before))
            total_time += time.time() - time_before
        if len(champs) < 1:
            print("Solution hasn't been found. Exiting.")
            return
        winner_gens = np.array(champs[0])
        if fails > 0:
            print('There were %d unseccessful simulations out of %d.' % (fails, nof_sims))
        print('\nOn average %0.1f generations are needed to find a solution' % np.mean(winner_gens))
        print('On average %d evaluations are needed to find a solution' %\
                int(np.mean(winner_gens*self.population_size)))
        print('The standard deviation for evaluations is %d' % np.std(winner_gens*self.population_size))
        print('The worst case was', np.amax(winner_gens))
        print('The best case was', np.amin(winner_gens))
        print('Average fitness', np.mean(champs[1]))

        hidden_nodes = np.array(list(map(lambda g : len(g.nodes), champs[2]))) - (self.in_dim + 1 +
                                                                                  self.out_dim)
        hidden_nodes_ave = np.mean(hidden_nodes)
        print('On average solution network had %0.2f hidden nodes' % hidden_nodes_ave)
        print('The standard deviaion for hidden nodes is %0.2f' % np.std(hidden_nodes))
        nondisabled_connections = np.mean(list(map(count_not_disabled_genes, champs[2])))
        hist = {}
        for i in hidden_nodes:
            if i not in hist:
                hist[i] = 1
            else:
                hist[i] += 1
        print(sorted(hist.items(), key=lambda x: x[0]))
        print('On average solution network had %0.2f nondisabled connection genes' %\
              nondisabled_connections)
        print('Time needed on average:', time_elapsed(total_time/nof_sims, raw=True))
        print('\nTotal time elapsed:', time_elapsed(total_time, raw=True))
        if self.save_best_sims:
            pickle.dump(champs, open('champs%0.1f' % np.amax(champs[1]), 'wb'))
        return champs


