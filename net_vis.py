import xmlrpc.client
import itertools
import numpy as np
import random
from population import Population
from genotype import Gene, Genotype


# Create an object to represent our server.
server_url = 'http://127.0.0.1:20738/RPC2'
server = xmlrpc.client.Server(server_url)
G = server.ubigraph

G.clear()

import time
nodes = {}
edges = set()
node_size_list = {}

font_size = "9"
sensor_node_c = '#00FF44'
bias_node_c = '#5C5CFF'
output_node_c = '#FA0000'
hidden_node_c = '#ABA474'
step_time = 0.08
step_function_time = 0.01

def smooth_size(v, size1, size2):
  for i in range(0, 5):
    G.set_vertex_attribute(v, "size", str(size1 + (i/4.0)*(size2-size1)))
    time.sleep(0.01)

def fun(from_neuron, to_neuron, node_color):

    if from_neuron in nodes:
        in_node = nodes[from_neuron]
    else:
        new_node = G.new_vertex()
        G.set_vertex_attribute(new_node, "color", node_color)
        nodes[from_neuron] = new_node
        G.set_vertex_attribute(new_node, "label", str(from_neuron))
        G.set_vertex_attribute(new_node, "fontsize", font_size)
        in_node = new_node

    if  to_neuron in nodes:
        out_node = nodes[to_neuron]
    else:
        new_node = G.new_vertex()
        G.set_vertex_attribute(new_node, "color", node_color)
        nodes[to_neuron] = new_node
        G.set_vertex_attribute(new_node, "label", str(to_neuron))
        G.set_vertex_attribute(new_node, "fontsize", font_size)
        out_node = new_node

    if (in_node, out_node) in edges:
        smooth_size(out_node, 1.0, 2.0)
        time.sleep(step_function_time)
        smooth_size(out_node, 2.0, 1.0)
        return
    e = G.new_edge(in_node, out_node)
    edges.add((in_node, out_node))
    G.set_edge_attribute(e, "arrow", "true")
    time.sleep(step_time)

def visualize(genotype, in_dim, out_dim):
    global nodes, edges, node_size_list
    G.clear()
    nodes = {}
    edges = set()
    node_size_list = {}
    for node in genotype.nodes.keys():
        new_node = G.new_vertex()
        if node < in_dim - 2:
            node_color = sensor_node_c
        elif node == in_dim - 1:
            node_color = bias_node_c
        elif node < in_dim + out_dim:
            node_color = output_node_c
        else:
            node_color = hidden_node_c
        G.set_vertex_attribute(new_node, "color", node_color)
        nodes[node] = new_node
        G.set_vertex_attribute(new_node, "label", str(node))
        G.set_vertex_attribute(new_node, "fontsize", font_size)
    for gene in genotype.genes.values():
        if gene.enabled:
            fun(gene.in_, gene.out, hidden_node_c)



