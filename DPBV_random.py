"""
My implementation of NEAT paper.
"""
import itertools
import numpy as np
import random
from population import Population
from genotype import Gene, Genotype
#from net_vis import visualize
import time
from net_vis2d import NEATVis
from neat import NeatLearning
import pickle
import imp
from math import radians as rad

dpmod = imp.load_source('double_pole_physics', '/home/saya/pygame_shit/double_pole_balancing_sim/double_pole_physics.py')
def dpb_factory():
    dpb = dpmod.PoledCart(2)
    dpb.pole_number = 2
    p_masses = [0.1]*dpb.pole_number
    p_angles = [0.0, rad(1.0)]
    p_h_lens = [0.1, 1.0]
    p_accels = [0.0]*dpb.pole_number
    p_vels = [0.0]*dpb.pole_number
    dpb.poles = []
    for i in range(dpb.pole_number):
        dpb.poles.append(dpmod.Pole(p_angles[i],
                         p_vels[i],
                         p_accels[i],
                         p_masses[i],
                         p_h_lens[i]))
    dpb.cart_pos = 0.0
    dpb.cart_vel = 0.0
    dpb.cart_acc = 0.0
    dpb.cart_mass = 1.0
    dpb.time = 0.0
    dpb.applied_force = 0.0
    dpb.track_limit = 2.4
    dpb.p_failure_angle = rad(36)
    dpb.time_step = 0.01
    dpb.cart_fric = 0.05
    dpb.p_fric = 0.000002
    dpb.stop_at_zero_deg = True
    return dpb

#in time steps ~30min - 100k of 0.02 - it seems they counted in
#time steps of a network outputing dorce which is 0.02
desirable_balancing_time = 100000

#cart + pole1 + pol2(pos + vel)
in_dim = 2 + 2 + 2
out_dim = 1
deg3_6 = rad(3.6)
deg8_6 = rad(8.6)
def get_normilized_dpb_input(dpb):
    return np.clip([dpb.cart_pos/dpb.track_limit,
            dpb.cart_vel/4.0,
            dpb.poles[0].angle/dpb.p_failure_angle,
            dpb.poles[0].vel/10.0,
            dpb.poles[1].angle/dpb.p_failure_angle,
            dpb.poles[1].vel/4.0], -1, 1)

def randomize_dpb(dpb):
    #short is still and in zero because he is already very fast
    p_angles = [0.0, random.random()*deg3_6*2 - deg3_6]
    p_vels = [0.0, random.random()*deg8_6*2 - deg8_6]
    dpb.cart_pos = random.random()*1.16*2 - 1.16
    dpb.cart_vel = random.random()*1.35*2 - 1.35
    for i in range(2):
        dpb.poles[i].angle = p_angles[i]
        dpb.poles[i].vel = p_vels[i]

def fitness_function(network):
    #dpb = dpb_factory()
    fitness = 0
    #max_a = [0, 0, 0, 0, 0, 0]
    for k in range(5):
        dpb = dpb_factory()
        randomize_dpb(dpb)
        network.reinit_network()
        for i in range(1, 5001):
            #print(get_normilized_dpb_input(dpb)[3])
            #max_a[0] = max(max_a[0], dpb.cart_pos)
            #max_a[1] = max(max_a[1], dpb.cart_vel)
            #max_a[2] = max(max_a[2], dpb.poles[0].angle)
            #max_a[3] = max(max_a[3], dpb.poles[0].vel)
            #max_a[4] = max(max_a[4], dpb.poles[1].angle)
            #max_a[5] = max(max_a[5], dpb.poles[1].vel)
            force = (network.feed(get_normilized_dpb_input(dpb))[0])*10
            dpb.applied_force = force
            dpb.update_state()
            dpb.update_state()
            if dpb.failed:
                break
        fitness += i
        dpb.failed = False
    #print(max_a)
    return fitness
#(self, nof_sims, in_dim, out_dim, population_size, fitness_f,
#desirable_fitness, recurrent=True, pop_params={}):
sim_number = 1
pop_params = {}
#pop_params['compat_th'] = 4.0
#pop_params['dist_coeffs'] = np.array([1.0, 1.0, 3.0])
neat = NeatLearning(sim_number, in_dim, out_dim, 200, fitness_function, 25000, True, multicore=True)
neat.max_gens = 100
neat.verbose = True
neat.start_simulations()
