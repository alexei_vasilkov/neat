from collections import deque
genes = {}
in_dim = 3
out_dim = 2
nodes = {}
order = {}
for i in range(in_dim + out_dim):
    nodes[i] = None

def add_gene(gene):
    if gene[0] not in genes:
        genes[gene[0]] = gene[1]
    if gene[0] not in nodes:
        nodes[gene[0]] = None
    if gene[1] not in nodes:
        nodes[gene[1]] = None

add_gene((0, 3))
add_gene((1, 4))
add_gene((2, 4))

to_visit = deque()

for i in range(in_dim):
    order[i\]
