"""
My implementation of NEAT paper.
"""
import itertools
import numpy as np
import random
from population import Population
from genotype import Gene, Genotype
#from net_vis import visualize
import time
from net_vis2d import NEATVis
from neat import NeatLearning
import pickle
import imp
from math import radians as rad

dpmod = imp.load_source('double_pole_physics', '/home/saya/pygame_shit/double_pole_balancing_sim/double_pole_physics.py')
def dpb_factory():
    dpb = dpmod.PoledCart(2)
    dpb.pole_number = 2
    p_masses = [0.1]*dpb.pole_number
    p_angles = [0.0, rad(1.0)]
    p_h_lens = [0.1, 1.0]
    p_accels = [0.0]*dpb.pole_number
    p_vels = [0.0]*dpb.pole_number
    dpb.poles = []
    for i in range(dpb.pole_number):
        dpb.poles.append(dpmod.Pole(p_angles[i],
                         p_vels[i],
                         p_accels[i],
                         p_masses[i],
                         p_h_lens[i]))
    dpb.cart_pos = 0.0
    dpb.cart_vel = 0.0
    dpb.cart_acc = 0.0
    dpb.cart_mass = 1.0
    dpb.time = 0.0
    dpb.applied_force = 0.0
    dpb.track_limit = 2.4
    dpb.p_failure_angle = rad(36)
    dpb.time_step = 0.01
    dpb.cart_fric = 0.05
    dpb.p_fric = 0.000002
    dpb.stop_at_zero_deg = True
    return dpb

#in time steps ~30min - 100k of 0.02 - it seems they counted in
#time steps of a network outputing dorce which is 0.02
desirable_balancing_time = 100000

#cart + pole1 + pol2(pos + vel)
in_dim = 2 + 2 + 2
out_dim = 1
def get_normilized_dpb_input(dpb):
    return np.clip([dpb.cart_pos/dpb.track_limit,
                     dpb.cart_vel/4.0,
                     dpb.poles[0].angle/dpb.p_failure_angle,
                     dpb.poles[0].vel/5.0,
                     dpb.poles[1].angle/dpb.p_failure_angle,
                     dpb.poles[1].vel/4.0], -1, 1)


def fitness_function(network):
    dpb = dpb_factory()
    for i in range(1, desirable_balancing_time+2):
        force = (network.feed(get_normilized_dpb_input(dpb))[0])*10
        dpb.applied_force = force
        dpb.update_state()
        dpb.update_state()
        if dpb.failed:
            return i
    return desirable_balancing_time
#(self, nof_sims, in_dim, out_dim, population_size, fitness_f,
#desirable_fitness, recurrent=True, pop_params={}):
sim_number = 1
neat = NeatLearning(sim_number, in_dim, out_dim, 150, fitness_function, 100000, True, multicore=True)
neat.max_gens = 100
neat.verbose = True
neat.start_simulations()
