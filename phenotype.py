"""
Here will be network from genotype generator
and creator
"""
from neurons import BiasNeuron, SensorNeuron, HiddenNeuron, OutputNeuron
from collections import OrderedDict, deque
import numpy as np
#TODO inputs normalization


def BFS(neurons, in_dim):
    visited = {}
    queue = deque()
    for i in range(in_dim):
        visited[i] = 0
        queue.append(i)

    while queue:
        now = queue.popleft()
        for node in neurons[now].outputs:
            if node in visited:
                continue
            queue.append(node)
            visited[node] = visited[now] + 1
    return sorted(visited.keys(), key=lambda x: visited[x])

class Network:

    def __init__(self, genotype, in_dim, out_dim, recurrent=True):
        self.neurons = OrderedDict()
        self.in_dim = in_dim
        self.out_dim = out_dim
        self.disable_recurrent_connections = not recurrent
        self.basic_neurons = in_dim + out_dim
        #generating a network
        for i in range(in_dim - 1):
            self.neurons[i] = SensorNeuron(i)
        self.neurons[in_dim - 1] = BiasNeuron(i)
        for i in range(in_dim, self.basic_neurons):
            self.neurons[i] = OutputNeuron(i)
        #TODO do we need this sort?
        for gene in sorted(genotype.genes.values(), key=lambda g: g.out):
        #for gene in genotype.genes.values():
            if gene.enabled:
                if gene.in_ not in self.neurons:
                    self.neurons[gene.in_] = HiddenNeuron(gene.in_)
                if gene.out not in self.neurons:
                    self.neurons[gene.out] = HiddenNeuron(gene.out)
                self.neurons[gene.out].inputs[gene.in_] = gene.weight
                self.neurons[gene.in_].outputs[gene.out] = None

        #self.order = BFS(self.neurons, in_dim)

    def activate_neuron(self, neuron):
        total_input = 0
        for in_neuron, weight in neuron.inputs.items():
            total_input += self.neurons[in_neuron].output_value * neuron.inputs[in_neuron]
        return neuron.activation(total_input)

    def reinit_network(self):
        for neuron in self.neurons.values():
            neuron.output_value = 0
        self.neurons[self.in_dim - 1].output_value = 1


    def feed(self, sample):
        if self.disable_recurrent_connections:
            self.reinit_network()
        for i in range(self.in_dim - 1):
            self.neurons[i].output_value = sample[i]
        for n_id, neuron in self.neurons.items():
            if not n_id < self.basic_neurons:
                self.activate_neuron(neuron)
        #for n_id in self.order:
        #    if not n_id < self.basic_neurons:
        #        self.activate_neuron(self.neurons[n_id])
        #reading outputs
        output = []
        for n_id in range(self.in_dim, self.basic_neurons):
            output.append(self.activate_neuron(self.neurons[n_id]))
        return np.array(output)
