from scipy.special import expit as sigmoid


UNDEFINED = 'Undefined'
BIAS = 'Bias'
SENSOR = 'Sensor'
HIDDEN = 'Hidden'
OUTPUT = 'Output'

class Neuron:

    def __init__(self, n_id):
        self.id = n_id
        self.type = UNDEFINED
        self.inputs = {}
        self.outputs = {}
        self.output_value = 0.0

class SensorNeuron(Neuron):

    def __init__(self, n_id):
        Neuron.__init__(self, n_id)
        self.type = SENSOR

    def activation(self, input_signal):
        return self.output_value

class BiasNeuron(Neuron):

    def __init__(self, n_id):
        Neuron.__init__(self, n_id)
        self.type = BIAS
        self.output_value = 1

    def activation(self, input_signal):
        self.output_value = 1
        return 1

class HiddenNeuron(Neuron):

    def __init__(self, n_id):
        Neuron.__init__(self, n_id)
        self.type = HIDDEN

    def activation(self, input_signal):
        self.output_value = 2*sigmoid(4.9*input_signal) - 1
        return self.output_value

class OutputNeuron(HiddenNeuron):

    def __init__(self, n_id):
        HiddenNeuron.__init__(self, n_id)
        self.type = OUTPUT
