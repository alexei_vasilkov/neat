import numpy as np
import itertools
from genotype import Gene, Genotype
import random
from collections import deque, OrderedDict
from phenotype import Network
import multiprocessing as mp

def choose_shortest_and_longest(q1, q2):
    if len(q1) > len(q2):
        return q2, q1
    else:
        return q1, q2

class Species:

    def __init__(self, representative,  id_, multicore=None):
        self.representative = representative.dummy_representative()
        self.champ = representative
        self.orgs = [representative]
        self.multicore = multicore
        self.fitness_ave = 0.0
        self.last_top_fitnesses = deque([0.0001]*15, maxlen=15)
        self.f15_gen_progress_ratio = 0.0
        self.id = id_
        self.gene_pool = {}

    def __len__(self):
        return len(self.orgs)

    def __hash__(self):
        return self.id

    def __eq__(self, other):
        return self.id == other.id

    def add(self, new_genotype):
        self.orgs.append(new_genotype)
        if self.champ.fitness < new_genotype.fitness:
            self.champ = new_genotype

    def __lt__(self, other):
        return self.champ.fitness < other.champ.fitness

    def evaluate(self, fitness_function):
        """
        must call it only once per generation!
        """
        self.fitness_ave = 0.0
        if self.multicore is None:
            for org in self.orgs:
                fitness_function(org)
                if org.fitness > self.champ.fitness:
                    self.champ = org
                self.fitness_ave += org.fitness
        else:
            with mp.Pool(mp.cpu_count()) as p:
                networks = (self.multicore[0])(self.orgs)
                fitnesses = p.map(self.multicore[1], networks)
                for i, org in enumerate(self.orgs):
                    org.fitness = fitnesses[i]
                    if org.fitness > self.champ.fitness:
                        self.champ = org
                    self.fitness_ave += org.fitness

        self.fitness_ave /= len(self)
        self.last_top_fitnesses.append(self.champ.fitness)
        #if it's less than th species will be killed
        self.f15_gen_progress_ratio = self.last_top_fitnesses[-1]/self.last_top_fitnesses[0]


class Population:
    """
    Here is structure that governs speciation, randomness, migration, etc.
    """
    #tested
    def __init__(self, size, in_dim, out_dim, fitness_f, recurrent=True, multicore=False):
        self.fitness_of_network = fitness_f
        self.recurrent = recurrent
        self._innovation = itertools.count()
        self._species_id = itertools.count()
        #(in, out) : innovation_number. used when adding genes
        #try with and without global pool
        #TODO maybe only for first generation whe zero new nodes and there are only ins+outs
        #self.gene_pool = {}
        self.population = []
        #minus out_dim
        #didn't force it anywhere though because it's unreachable for usual use
        #c1,c2,c3
        self.dist_coeffs = np.array([1.0, 1.0, 0.4])
        #3d param is larger if large population
        #because it has more room to distinguish by weights
        #and small more by topology
        self.compat_th = 3.0
        #percent of individuals to reproduce in each species
        self.champs_percent_to_reproduce = 0.4
        #plus bias but it'snot guaranteed to connect to all neurons just like inputs
        self.in_dim = in_dim + 1
        self.out_dim = out_dim
        self.size = size
        self.pop_species = OrderedDict()

        self.last_top_fitnesses = deque([0.0001]*20, maxlen=20)
        self.f20_gen_progress_ratio = 0.0

        self.mutations_chances = {}
        #depends on population size
        #theirs 0.05 and 0.03
        self.mutations_chances['add_connection'] = 0.05
        self.mutations_chances['add_node'] = 0.03

        self.mutations_chances['mutate_weights'] = 0.8
        self.mutations_chances['perturb_weight'] = 0.9
        self.mutations_chances['disabled_inheritance'] = 0.75
        self.mutations_chances['interspecies_mating'] = 0.001
        self.offspring_percent_wo_crossover = 0.25
        self.perturbation_step = 0.1
        #plus bias
        self.genotype_init_tuple = self.in_dim + self.out_dim
        #if for 15 gens species didn't improve for more than 0.01 percent(1.0001)
        self.progress_ratio_th_to_kill_species = 1.0001
        #threshold when population is considered stale
        self.progress_ratio_th_for_martial_law = 1.0001
        self.dummy_zero_fitness_genotype = Genotype(0)
        self.dummy_zero_fitness_genotype.fitness = 0
        self.champ = self.dummy_zero_fitness_genotype
        if multicore:
            self.multi_core_tuple = (self.gens_into_phens, fitness_f)
        else:
            self.multi_core_tuple = None
        #w/o reccurrent connections
        #just use in<out rule
        #with rec add backward passing before forward
        self.init_population()
    #tested
    def __len__(self):
        return len(self.population)

    def change_default_params(self, new_params):
        for key in new_params:
            setattr(self, key, new_params[key])

    def get_the_most_fittest(self):
        return self.champ

    def get_max_fitness(self):
        return self.champ.fitness

    #tested
    def init_population(self):
        """
        initializing the population
        """
        #maybe randomly connect every input to random output
        #TODO implement good weight initialization
        gene_pool = {}

        for i in range(self.size):
            self.population.append(Genotype(self.genotype_init_tuple))
            for i in range(self.in_dim - 1):
                gene_key = (i, random.randrange(self.in_dim, self.in_dim + self.out_dim))
                gene = Gene(gene_key, random.random()*2 - 1, True, self.get_gene_innovation(gene_key, gene_pool))
                self.population[-1].add_gene(gene)
            for out in range(self.in_dim, self.in_dim + self.out_dim):
                gene_key = (self.in_dim - 1, out)
                gene = Gene(gene_key, random.random()*2 - 1, True, self.get_gene_innovation(gene_key, gene_pool))
                self.population[-1].add_gene(gene)

    #tested
    def compute_distance(self, genotype1, genotype2):
        """
        computing distance for the speciation
        """
        return self.dist_coeffs.dot(genotype1.get_distance_values(genotype2))

    def gens_into_phens(self, population):
        return [Network(genotype, self.in_dim, self.out_dim, self.recurrent) for genotype in population]

    #tested
    def fitness_f(self, genotype):
        """
        fitness_function
        """
        network = Network(genotype, self.in_dim, self.out_dim, self.recurrent)
        genotype.fitness = self.fitness_of_network(network)
        #genotype.fitness = random.random()*20
        return genotype.fitness
    #hope it works
    def speciate(self):
        """
        speciates population
        """
        if len(self) != self.size:
            print(len(self))
            print('Wrong population size')
            raise
        #list of lists - each list - one species
        if len(self) < 1:
            print('Your population has zero genotypes.')
            return
        ave_len = 0
        for genotype in self.population:
            ave_len += len(genotype)
            added = False
            for species in self.pop_species:
                if self.compute_distance(genotype, species.representative) < self.compat_th:
                    species.add(genotype)
                    added = True
                    break
            if not added:
                self.pop_species[Species(genotype, next(self._species_id), self.multi_core_tuple)] = None
        ave_len /= self.size
        print('Average genotype len: %0.1f' % ave_len)

        #filter dead species
        dead_species = []
        for species in self.pop_species:
            if len(species) == 0:
                dead_species.append(species)
                continue
            species.gene_pool = {}
            species.representative = random.choice(species.orgs).dummy_representative()
        for species in dead_species:
            del self.pop_species[species]
        print('Speciation:', len(self.pop_species))

    def node_mutation(self, genotype, gene_pool):
        """
        adds a new node if it's lucky
        """
        if len(genotype) == 0:
            return

        #initial_connection = random.choice(list(genotype.genes.keys()))
        initial_connection = random.choice(genotype.genes_in_list)
        #disabling splitted connection
        genotype.change_gene_expression(initial_connection, False)
        new_node = genotype.add_node()
        gene_key = (initial_connection[0], new_node)
        pre_split_gene = Gene(gene_key, 1.0, True,
                              self.get_gene_innovation(gene_key, gene_pool))
        gene_key = (new_node, initial_connection[1])
        post_split_gene = Gene(gene_key, genotype.genes[initial_connection].weight,
                               True, self.get_gene_innovation(gene_key, gene_pool))
        genotype.add_gene(pre_split_gene)
        genotype.add_gene(post_split_gene)

        genotype.firing_order[new_node] = initial_connection
        #TODO HERE

        return pre_split_gene.hash_key, post_split_gene.hash_key

    def mutate_weights(self, genotype):
        for gene in genotype.genes.values():
            if self.chance('perturb_weight'):
                gene.weight += random.random()*2*self.perturbation_step - self.perturbation_step
            else:
                #TODO good random initializatiion
                gene.weight = random.random()*2 - 1

    def connection_mutation(self, genotype, gene_pool):
        """
        adds a new connection if it's lucky
        """
        #nodes = list(genotype.nodes.keys())
        nodes = genotype.nodes_in_list
        from_neuron = random.choice(nodes)
        to_neuron = random.choice(nodes)
        if from_neuron < self.in_dim and to_neuron < self.in_dim:
            #both are input nodes
            return
        if to_neuron < self.in_dim:
            from_neuron, to_neuron = to_neuron, from_neuron
        gene_key = (from_neuron, to_neuron)
        if gene_key in genotype.genes:
            return
        #TODO good random initialization
        new_gene = Gene(gene_key, random.random()*2 - 1, True,
                        self.get_gene_innovation(gene_key, gene_pool))
        genotype.add_gene(new_gene)
        return gene_key

    def get_gene_innovation(self, gene_key, gene_pool):
        if gene_key not in gene_pool:
            gene_pool[gene_key] = next(self._innovation)
        return gene_pool[gene_key]
    #IIRC tested
    def crossover(self, genotype1, genotype2):
        shortest, longest = choose_shortest_and_longest(genotype1, genotype2)
        #TODO be careful with migrating through species
        #simplest but fittest will become complex and vice versa
        matching_innovations = []
        for innov in shortest.genes_by_innov:
            if innov in longest.genes_by_innov:
                matching_innovations.append(innov)

        new_genotype = Genotype(self.genotype_init_tuple)

        genotypes = (genotype1, genotype2)
        #TODO what about nodes?
        for innov in matching_innovations:
            #IMPORTANT gene.copy()
            #TODO if either parent's gene is disabled
            #THERE'IS a chance kid will have a disabled gene. not 100% even it chosen disabled?
            gene = genotypes[random.randint(0, 1)].genes_by_innov[innov].copy()
            if not (genotype1.genes_by_innov[innov].enabled and
                    genotype2.genes_by_innov[innov].enabled):
                if self.chance('disabled_inheritance'):
                    gene.enabled = False
                #elif genotype1.genes_by_innov[innov].enabled != genotype2.genes_by_innov[innov].enabled:
                #    gene.enabled = True

            new_genotype.add_gene(gene)

        matching_innovations = set(matching_innovations)

        if shortest.fitness > longest.fitness or shortest.fitness == longest.fitness:
            disjoint_and_excess_innovs = set(shortest.genes_by_innov.keys())
            disjoint_and_excess_innovs -= matching_innovations
            for innov in disjoint_and_excess_innovs:
                new_genotype.add_gene(shortest.genes_by_innov[innov].copy())

        if shortest.fitness < longest.fitness or shortest.fitness == longest.fitness:
            disjoint_and_excess_innovs = set(longest.genes_by_innov.keys())
            disjoint_and_excess_innovs -= matching_innovations
            for innov in disjoint_and_excess_innovs:
                new_genotype.add_gene(longest.genes_by_innov[innov].copy())

        return new_genotype


    def evaluate_population(self):
        for species in self.pop_species:
            species.evaluate(self.fitness_f)

    def next_gen(self):
        #IMPORTANT steps
        self.speciate()
        self.evaluate_population()

        self.mate_them_all()

    def mate_them_all(self):
        """
        here's mating and species reorganization using
        explicit fitness sharing
        """
        #TODO
        #if over 20 gens fitness of the entire pop doesn't improve
        #only top 2 of each species are allowed to reproduce

        species_fitness_aves = []
        species_to_kill = []
        self.champ = self.dummy_zero_fitness_genotype
        two_most_promising_species = [[-1, None], [-1, None]]
        for species in self.pop_species:
            if self.champ.fitness < species.champ.fitness:
                self.champ = species.champ
            if two_most_promising_species[0][0] < species.fitness_ave:
                two_most_promising_species[1][0] = two_most_promising_species[0][0]
                two_most_promising_species[1][1] = two_most_promising_species[0][1]
                two_most_promising_species[0][0] = species.fitness_ave
                two_most_promising_species[0][1] = species
            elif two_most_promising_species[1][0] < species.fitness_ave:
                two_most_promising_species[1][0] = species.fitness_ave
                two_most_promising_species[1][1] = species
            if species.f15_gen_progress_ratio < self.progress_ratio_th_to_kill_species:
                species_to_kill.append(species)
        self.last_top_fitnesses.append(self.champ.fitness)
        self.f20_gen_progress_ratio = self.last_top_fitnesses[-1]/self.last_top_fitnesses[0]
        for species in species_to_kill:
            if species.champ.fitness == self.champ.fitness:
                continue
            #print('Killing someone')
            del self.pop_species[species]
        if len(self.pop_species) == 0:
            #can't happen
            print("We've killed everybody, master.")
            self.population = []
            return
        species_fitness_aves = []
        for species in self.pop_species:
            species_fitness_aves.append(species.fitness_ave)

        species_fitness_aves = np.array(species_fitness_aves)

        new_species_lengths = species_fitness_aves/(np.sum(species_fitness_aves)/len(self))

        #make sure sum is self.size
        #somewhat tested

        new_species_lengths = np.rint(new_species_lengths).astype(int)
        if self.progress_ratio_th_for_martial_law > self.f20_gen_progress_ratio:
            print('Martial law shit')
            self.pop_species = OrderedDict([(two_most_promising_species[0][1], None),
                                            (two_most_promising_species[1][1], None)])
            new_species_lengths = np.rint([0.6*self.size, 0.4*self.size]).astype(int)
            if two_most_promising_species[1][1] is None:
                self.pop_species = OrderedDict([(two_most_promising_species[0][1], None)])
                new_species_lengths = [self.size]


        for i in range(len(new_species_lengths)):
            if new_species_lengths[i] == 0:
                new_species_lengths[i] = 1
        new_size = np.sum(new_species_lengths)
        while new_size != self.size:
            pick = random.randrange(0, len(new_species_lengths))
            if new_size < self.size:
                new_species_lengths[pick] += 1
                new_size += 1
            elif new_size > self.size:
                if new_species_lengths[pick] == 1:
                    if new_size != len(new_species_lengths):
                        for i in range(len(new_species_lengths)):
                            #if all elements are 1 can't possibly have new_size > self.size,
                            #right?
                            if new_species_lengths[i] > 1:
                                pick = i
                                break
                new_species_lengths[pick] -= 1
                new_size -= 1
        #fitnesses have already been updated and cached into genotypes
        #not adjusted fitnesses though
        #TODO make sure it will produce the same number of organisms in total
        population = []
        champs = []
        interspecies_mating_wonderers = []

        for species, new_len in zip(self.pop_species, new_species_lengths):
            offspring, champions, interspecies_mating_candidates = self.produce_next_species_gen(species, new_len)
            population += offspring
            champs += champions
            interspecies_mating_wonderers += interspecies_mating_candidates
        #interspeciesmating
        for champ in interspecies_mating_wonderers:
            population.append(self.crossover(champ, random.choice(champs)))
            self.mutate_genotype(population[-1], {})
        self.population = population


    def chance(self, mutation_chance_name):
        return self.mutations_chances[mutation_chance_name] > random.random()

    def mutate_genotype(self, genotype, gene_pool):
        if self.chance('mutate_weights'):
            self.mutate_weights(genotype)
        if self.chance('add_connection'):
            self.connection_mutation(genotype, gene_pool)
        if self.chance('add_node'):
            self.node_mutation(genotype, gene_pool)

    def produce_next_species_gen(self, species, new_len):
        #TODO interspecies mating
        #gene_pool to produce the same inno numbers inside the same species
        offspring = []
        nof_champs = round(len(species) * self.champs_percent_to_reproduce)
        if nof_champs < 1:
            nof_champs = 1
        #IMPORTANT SORT INPLACE
        species.orgs.sort(key=lambda genotype : genotype.fitness, reverse=True)
        champions = species.orgs[:nof_champs]
        n_wo_crossover = int(self.offspring_percent_wo_crossover*new_len)
        t = 0
        for i in range(n_wo_crossover):
            offspring.append(champions[t].copy(self.genotype_init_tuple))
            t += 1
            if t == nof_champs:
                t = 0
        #adding champ w/o changes in the end
        if len(species) > 5:
            n_wo_crossover += 1
        want_to_mate_with_other_species = []
        for i in range(new_len - n_wo_crossover):
            if self.chance('interspecies_mating'):
                want_to_mate_with_other_species.append(random.choice(champions))
        want_to_mate_with_other_species = []
        for i in range(new_len - n_wo_crossover - len(want_to_mate_with_other_species)):
            offspring.append(self.crossover(random.choice(champions), random.choice(champions)))

        for genotype in offspring:
            self.mutate_genotype(genotype, species.gene_pool)

        if len(species) > 5:
            offspring.append(species.champ)

        species.champ = self.dummy_zero_fitness_genotype
        species.fitness_ave = 0
        species.orgs = []

        return offspring, champions, want_to_mate_with_other_species
