"""
My implementation of NEAT paper.
"""
import itertools
import numpy as np
import random
from population import Population
from genotype import Gene, Genotype
#from net_vis import visualize
import time
from net_vis2d import NEATVis
from neat import NeatLearning
import pickle

in_dim = 2
out_dim = 1
samples = [[0, 1], [1, 1], [1, 0], [0, 0]]
samples = list(map(np.array, samples))
targets = [1, 0, 1, 0]

champs = [[], []]
def fitness_function(network):
    #global targets, samples
    fitness = 4
    #np.random.shuffle(samples)
    #idx = list(range(4))
    #np.random.shuffle(idx)
    #samples_sh = []
    #targets_sh = []
    #for i in idx:
    #    samples_sh.append(samples[i])
    #    targets_sh.append(targets[i])
    #samples = samples_sh
    #targets = targets_sh
    for input_vector, target in zip(samples, targets):
        fitness -= abs(target - (network.feed(input_vector)[0] > 0))
    return fitness**2
#(self, nof_sims, in_dim, out_dim, population_size, fitness_f,
#desirable_fitness, recurrent=True, pop_params={}):
sim_number = 100
neat = NeatLearning(sim_number, in_dim, out_dim, 150, fitness_function, 16, True)
neat.start_simulations()

"""
for sim_id in range(sim_number):
    print('Running simulation', sim_id)
    #pop = Population(150, in_dim, out_dim, fitness_function, False)
    pop = Population(150, in_dim, out_dim, fitness_function, True)
    for gen_id in range(300):
        pop.next_gen()
        if pop.champ.fitness == 16:
            champs[0].append(gen_id)
            champs[1].append(pop.champ)
            break

    if len(champs[0]) != sim_id + 1:
        print('Unseccessful run', sim_id)
        champs[0].append(0)
        champs[1].append(None)

pickle.dump(champs, open('xor_test_results.p', 'wb'))

winner_gens = np.array(champs[0])
print('On average %0.1f generations are needed to find a solution' % np.mean(winner_gens))
print('The worst case was', np.amax(winner_gens))
print('The best case was', np.amin(winner_gens))

def count_not_disabled_genes(genotype):
    res = 0
    for gene in genotype.genes.values():
        if gene.enabled:
            res += 1
    return res
hidden_nodes = np.array(list(map(lambda g : len(g.nodes), champs[1]))) - (in_dim + 1 + out_dim)
hidden_nodes_ave = np.mean(hidden_nodes)
print('On average solution network had %0.2f hidden nodes' % hidden_nodes_ave)
print('The standard deviaion for hidden nodes is %0.2f' % np.std(hidden_nodes))
nondisabled_connections = np.mean(list(map(count_not_disabled_genes, champs[1])))
hist = {}
for i in hidden_nodes:
    if i not in hist:
        hist[i] = 1
    else:
        hist[i] += 1
print(sorted(hist.items(), key=lambda x: x[0]))
print('On average solution network had %0.2f nondisabled connection genes' %\
      nondisabled_connections)
"""
