"""
My implementation of NEAT paper.
"""
import itertools
import numpy as np
import random
from population import Population
from genotype import Gene, Genotype
#from net_vis import visualize
import time
from net_vis2d import NEATVis
from neat import NeatLearning
import pickle
import imp
from math import radians as rad
from collections import deque

dpmod = imp.load_source('double_pole_physics', '/home/saya/pygame_shit/double_pole_balancing_sim/double_pole_physics.py')
track_limit = 2.4
p_failure_angle = rad(36)

def dpb_factory():
    dpb = dpmod.PoledCart(2)
    dpb.pole_number = 2
    p_masses = [0.1]*dpb.pole_number
    p_angles = [0.0, rad(4.5)]
    p_h_lens = [0.05, 0.5]
    p_accels = [0.0]*dpb.pole_number
    p_vels = [0.0]*dpb.pole_number
    dpb.poles = []
    for i in range(dpb.pole_number):
        dpb.poles.append(dpmod.Pole(p_angles[i],
                         p_vels[i],
                         p_accels[i],
                         p_masses[i],
                         p_h_lens[i]))
    dpb.cart_pos = 0.0
    dpb.cart_vel = 0.0
    dpb.cart_acc = 0.0
    dpb.cart_mass = 1.0
    dpb.time = 0.0
    dpb.applied_force = 0.0
    dpb.track_limit = track_limit
    dpb.p_failure_angle = p_failure_angle
    dpb.time_step = 0.01
    dpb.cart_fric = 0.05
    dpb.p_fric = 0.000002
    dpb.stop_at_zero_deg = True
    return dpb

#in time steps ~30min - 100k of 0.02 - it seems they counted in
#time steps of a network outputing dorce which is 0.02
desirable_balancing_time = 100000

#cart + pole1 + pol2(pos + vel)
in_dim = 1 + 2
out_dim = 1
def get_normilized_dpb_input(dpb):
    return np.clip([dpb.cart_pos/dpb.track_limit,
                     dpb.poles[0].angle/dpb.p_failure_angle,
                     dpb.poles[1].angle/dpb.p_failure_angle], -1, 1)

perturb_vals_initial_state = [-0.9, -0.5, 0, 0.5, 0.9]
def reinit_dpb(dpb):
    for i in range(dpb.pole_number):
        dpb.poles[i].vel = 0.0
        dpb.poles[i].angle = 0.0
        dpb.poles[i].acc = 0.0
    dpb.poles[1].angle = rad(4.5)
    dpb.cart_pos = 0.0
    dpb.cart_vel = 0.0
    dpb.cart_acc = 0.0
    dpb.applied_force = 0.0
    dpb.failed = False

def perturb_dpb(dpb, q1, q2, q3, q4):
    dpb.cart_pos = 2.16*q1
    dpb.cart_vel = 1.35*q2
    #long one
    dpb.poles[0].angle = 0
    dpb.poles[0].vel = 0
    dpb.poles[1].angle = rad(3.6)*q3
    dpb.poles[1].vel = rad(8.6)*q4

def champ_fitness_function(network):
    dpb = dpb_factory()
    total = 625
    info = 0
    for q1 in perturb_vals_initial_state:
        for q2 in perturb_vals_initial_state:
            for q3 in perturb_vals_initial_state:
                for q4 in perturb_vals_initial_state:
                    reinit_dpb(dpb)
                    perturb_dpb(dpb, q1, q2, q3, q4)
                    network.reinit_network()
                    #print(q1,q2,q3,q4)
                    for i in range(0, 1001):
                        force = (network.feed(get_normilized_dpb_input(dpb))[0])*10
                        dpb.applied_force = force
                        dpb.update_state()
                        dpb.update_state()
                        if dpb.failed:
                            total -= 1
                            break
                    info += i
                    #if i == 1000:
                    #    print(q1,q2,q3,q4)
    print('Champ seccessfully completed %d generalization tests' % total)
    print('Small info', info)
    if total < 200:
        return False
    dpb = dpb_factory()
    network.reinit_network()
    for i in range(0, desirable_balancing_time+2):
        force = (network.feed(get_normilized_dpb_input(dpb))[0])*10
        dpb.applied_force = force
        dpb.update_state()
        dpb.update_state()
        if dpb.failed:
            print("Champ wasn't able to complete 100k trip")
            print('Failed at %d step' % i)
            return False
    print("Champ's successfully completed 100k trip")
    return True

def general_fitness_function(network):
    dpb = dpb_factory()
    values = [deque([], maxlen=100),
              deque([], maxlen=100),
              deque([], maxlen=100),
              deque([], maxlen=100)]
    for i in range(0, 1001):
        force = (network.feed(get_normilized_dpb_input(dpb))[0])*10
        dpb.applied_force = force
        dpb.update_state()
        dpb.update_state()
        values[0].append(dpb.cart_pos)
        values[1].append(dpb.cart_vel)
        #long one only
        values[2].append(dpb.poles[1].angle)
        values[3].append(dpb.poles[1].vel)
        if dpb.failed:
            break
    f1 = i/1000
    if i < 100:
        f2 = 0
    else:
        f2 = 0.75/np.sum(np.fabs(values))
    return (0.1*f1 + 0.9*f2)*100


#(self, nof_sims, in_dim, out_dim, population_size, fitness_f,
#desirable_fitness, recurrent=True, pop_params={}):

pop_params = {}
pop_params['compat_th'] = 4.0
pop_params['dist_coeffs'] = np.array([1.0, 1.0, 3.0])
mutations_chances = {}
mutations_chances['add_connection'] = 0.3
pop_params['mutations_chances'] = mutations_chances

sim_number = 3
neat = NeatLearning(sim_number, in_dim, out_dim, 1000, general_fitness_function, None, True,
                    multicore=True, pop_params=pop_params, check_for_champ=champ_fitness_function)
neat.max_gens = 500
neat.verbose = True
neat.start_simulations()

